# M431-Microhard
## Herzlich willkommen im Haupt-readme

### Unsere Webseiten
-  Die Website von [Robin Rüegg](https://robinrueegg.ch/)
-  Die Website von [Elia Weber](https://eliaweber.ch/)

### Navigationsbereich 

Unser Modul ist mit einer Dokumentation aufgebaut, welche sich nach dem IPERKA Modell aufbaut. 

- [Informieren](/01_I/)
- [Planen](/02_P/)
- [Entscheiden](/03_E/)
- [Realisieren](/04_R/)
- [Kontrollieren](/05_K/)
- [Auswerten](/06_A/)