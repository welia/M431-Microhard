**Elementor installieren**
Um die Webseite nicht mit dem eigenen Tool von Wordpress zu bearbeiten kann man elementor installieren. Die normale Version von Elementor ist kostenlos, jedoch gibt es auch noch eine pro Vesion welche pro Aktivierung 33.- kostet. 

**Installation**
Zuerst braucht man ein Hosting, installiertes WordPress, ein heruntergeladenes und aktiviertes Theme, Wenn man den Elementor-Builder noch nicht installiert hat, muss man auf Plugins> Add new und dann das Wort "elementor" in die Suchleiste eintippen. Dann auf den Button "Jetzt installieren" klicken und den Elementor aktivieren.

![wordpress-plugins-installieren](Bilder/../../Bilder/wordpress-plugins-installieren.png)

Jetzt kann man Seiten oder Beiträge auch via Elementor bearbeiten.
![wordpress-seite-mit-elementor-bearbeiten.png](Bilder/../../Bilder/wordpress-seite-mit-elementor-bearbeiten.png)